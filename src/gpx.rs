// SPDX-License-Identifier: MIT
//
// TrailCorder Authors: see AUTHORS.txt

use mk_geolocation::GeolocationPosition;

use gpx::{Gpx, GpxCopyright, GpxVersion, Link, Metadata, Track, TrackSegment, Waypoint};

use chrono::{DateTime, Utc};

pub fn new_gpx() -> Gpx {
    let gpx_spec = Link {
        href: "https://www.topografix.com/gpx.asp".into(),
        text: Some("GPX Spec".into()),
        ..Default::default()
    };

    let copyright = GpxCopyright {
        author: Some("TrailCorder".into()),
        year: Some(2021),
        ..Default::default()
    };

    let meta = Metadata {
        time: Some(Utc::now()),
        links: vec![gpx_spec],
        copyright: Some(copyright),
        ..Default::default()
    };

    let track = Track {
        segments: vec![TrackSegment::new()],
        ..Default::default()
    };

    Gpx {
        version: GpxVersion::Gpx11,
        creator: Some("TrailCorder".into()),
        metadata: Some(meta),
        tracks: vec![track],
        ..Default::default()
    }
}

pub fn geo_position_to_waypoint(position: &GeolocationPosition) -> Waypoint {
    let mut waypoint = Waypoint::new(geo_types::Point::new(
        position.coords().longitude(),
        position.coords().latitude(),
    ));
    waypoint.elevation = position.coords().altitude();
    // from the spec:
    // "Let acquisitionTime be a new DOMTimeStamp that represents now in milliseconds, using 01 January, 1970 UTC as the epoch."
    let ms = position.timestamp() as i64;
    let naive_t = chrono::NaiveDateTime::from_timestamp(ms / 1000, ((ms % 1000) * 1000) as u32);
    let time: DateTime<Utc> = DateTime::from_utc(naive_t, Utc);
    waypoint.time = Some(time);
    waypoint
}

impl crate::db::DbKeyComp for gpx::Gpx {
    fn db_key_cmp(&self, other: &Self) -> std::cmp::Ordering {
        use std::cmp::Ordering;
        if self.metadata.is_none() && other.metadata.is_none() {
            return Ordering::Equal;
        } else if self.metadata.is_none() && other.metadata.is_some() {
            return Ordering::Less;
        } else if self.metadata.is_some() && other.metadata.is_none() {
            return Ordering::Greater;
        } else {
            let meta_self = self.metadata.as_ref().unwrap();
            let meta_other = other.metadata.as_ref().unwrap();
            if meta_self.time.is_none() && meta_other.time.is_none() {
                return Ordering::Equal;
            } else if meta_self.time.is_none() && meta_other.time.is_some() {
                return Ordering::Less;
            } else if meta_self.time.is_some() && meta_other.time.is_none() {
                return Ordering::Greater;
            } else {
                let time_self = meta_self.time.unwrap();
                let time_other = meta_other.time.unwrap();
                return time_self.cmp(&time_other);
            }
        }
    }
}
