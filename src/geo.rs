// SPDX-License-Identifier: MIT
//
// TrailCorder Authors: see AUTHORS.txt

use seed::prelude::*;

use futures_util::stream::StreamExt;

#[derive(Debug, PartialEq, Eq)]
pub enum GeoApiMode {
    Callback,
    Future,
}

impl GeoApiMode {
    pub(crate) fn create_geolocator<O, E, H, Msg: 'static>(
        &self,
        options: mk_geolocation::PositionOptions,
        handler_ok: O,
        handler_err: E,
        orders: &mut impl Orders<crate::Message>,
        handler: H,
    ) -> Box<dyn Geolocator>
    where
        O: 'static + FnMut(mk_geolocation::GeolocationPosition),
        E: 'static + FnMut(mk_geolocation::GeolocationPositionError),
        H: FnOnce(mk_geolocation::future::GeolocationPositionResult) -> Msg + Clone + 'static,
    {
        match self {
            Self::Callback => GeolocatorCallback::new(options, handler_ok, handler_err),
            Self::Future => GeolocatorFuture::new(options, orders, handler),
        }
    }
}

pub trait Geolocator {
    // TODO: consume self
    fn stop(&self);
}

struct GeolocatorCallback {
    watch_id: i32,
}

impl GeolocatorCallback {
    pub fn new<O, E>(
        options: mk_geolocation::PositionOptions,
        handler_ok: O,
        handler_err: E,
    ) -> Box<dyn Geolocator>
    where
        O: 'static + FnMut(mk_geolocation::GeolocationPosition),
        E: 'static + FnMut(mk_geolocation::GeolocationPositionError),
    {
        let watch_id =
            mk_geolocation::callback::ContinuousPosition::new_with_error_callback_and_options(
                handler_ok,
                handler_err,
                options,
            )
            .forget();

        Box::new(GeolocatorCallback { watch_id })
    }
}

impl Geolocator for GeolocatorCallback {
    fn stop(&self) {
        mk_geolocation::callback::ContinuousPosition::clear_watch(self.watch_id);
    }
}

impl Drop for GeolocatorCallback {
    fn drop(&mut self) {
        self.stop();
    }
}

struct GeolocatorFuture {
    geo_pos_stream_handle: seed::app::StreamHandle,
}

impl GeolocatorFuture {
    pub(crate) fn new<H, Msg: 'static>(
        options: mk_geolocation::PositionOptions,
        orders: &mut impl Orders<crate::Message>,
        handler: H,
    ) -> Box<dyn Geolocator>
    where
        H: FnOnce(mk_geolocation::future::GeolocationPositionResult) -> Msg + Clone + 'static,
    {
        let stream_handle = orders.stream_with_handle(
            mk_geolocation::future::PositionStream::new_with_options(options)
                .map(move |pos_result| handler.clone()(pos_result)),
        );

        Box::new(GeolocatorFuture {
            geo_pos_stream_handle: stream_handle,
        })
    }
}

impl Geolocator for GeolocatorFuture {
    fn stop(&self) {
        // dropping the stream handle calls ::cancel() on the stream object
        drop(&self.geo_pos_stream_handle);
    }
}

impl Drop for GeolocatorFuture {
    fn drop(&mut self) {
        self.stop();
    }
}
