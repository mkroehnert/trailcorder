// SPDX-License-Identifier: MIT
//
// TrailCorder Authors: see AUTHORS.txt

use seed::prelude::*;

mod db;
mod export;
mod geo;
mod gpx;
mod map;
mod model;
mod widgets;

use export::download_gpx;
use geo::GeoApiMode;
use model::{GpsState, Model, MAP_DIV_ID};

#[wasm_bindgen(start)]
pub fn start() {
    App::start("app", init, update, view);
}

/// initialize model and schedule map view initialization
fn init(_: Url, orders: &mut impl Orders<Message>) -> Model {
    // Leaflet must be initialized after the map element has rendered.
    orders.after_next_render(move |_| Message::InitMap(map::Map::new_on(MAP_DIV_ID)));

    let mut position_options = mk_geolocation::PositionOptions::new();
    position_options.enable_high_accuracy(true);

    Model::new(position_options)
}

/// construct the main application which is rendered by Seed
fn view(model: &Model) -> impl IntoNodes<Message> {
    widgets::trailcorder_main(
        &model,
        MAP_DIV_ID,
        Message::GpsOnOff,
        // preferences
        Message::TogglePreferences,
        Message::ToggleGpsMode,
        // track list
        Message::ToggleTrackList,
        Message::SelectTrack,
        Message::ExportTrackAsGpx,
        Message::DbClear,
    )
}

/// handle messages and update application state
fn update(msg: Message, model: &mut Model, orders: &mut impl Orders<Message>) {
    match msg {
        Message::GpsOnOff() => {
            if model.geo_locator.is_none() {
                orders.send_msg(Message::GpsContinuous());
            } else {
                orders.send_msg(Message::GpsContinuousStop());
            }
        }
        Message::ToggleGpsMode() => {
            if GeoApiMode::Callback == model.geo_api_mode {
                model.geo_api_mode = GeoApiMode::Future;
            } else {
                model.geo_api_mode = GeoApiMode::Callback;
            }
            orders.send_msg(Message::Log(format!(
                "Switch Mode: {:?}",
                model.geo_api_mode
            )));
        }
        Message::GpsContinuous() => {
            if model.geo_locator.is_some() {
                return;
            };

            let (ok_app, ok_msg_mapper) = (orders.clone_app(), orders.msg_mapper());
            let (err_app, err_msg_mapper) = (orders.clone_app(), orders.msg_mapper());

            model.geo_locator = Some(model.geo_api_mode.create_geolocator(
                model.geo_options.clone(),
                // callback ok function
                move |position: mk_geolocation::GeolocationPosition| {
                    ok_app.update(ok_msg_mapper(Message::GeoPosition(position)));
                },
                // callback err function
                move |err: mk_geolocation::GeolocationPositionError| {
                    err_app.update(err_msg_mapper(Message::GeoPositionError(err)));
                },
                orders,
                // future handler function
                move |res: mk_geolocation::future::GeolocationPositionResult| {
                    res.map_or_else(
                        |e| Message::GeoPositionError(e),
                        |p| Message::GeoPosition(p),
                    )
                },
            ));

            model.reset_track();
            model.gps_state = GpsState::Waiting;

            match model.geo_api_mode {
                GeoApiMode::Callback => {
                    orders.send_msg(Message::Log("Info: trigger GPS continuous CB".into()));
                    orders.skip();
                }
                GeoApiMode::Future => {
                    orders.send_msg(Message::Log("Info: trigger GPS continuous Fut".into()));
                    orders.skip();
                }
            };
        }
        Message::GpsContinuousStop() => {
            if let Some(locator) = model.geo_locator.take() {
                locator.stop();
                let _ = model.store_track_in_db();
            };
            model.gps_state = GpsState::Off;
        }
        Message::GeoPosition(pos) => {
            orders.skip();
            orders.send_msg(Message::Log(format!(
                "{} {} {} {:?}",
                &pos.timestamp(),
                &pos.coords().latitude(),
                &pos.coords().longitude(),
                &pos.coords().altitude(),
            )));

            model.add_current_position(&pos);
            model.gps_state = GpsState::On;
        }
        Message::GeoPositionError(position_error) => {
            orders.skip();
            match position_error.code() {
                mk_geolocation::GeolocationPositionError::PERMISSION_DENIED => {
                    orders.send_msg(Message::GpsContinuousStop());
                    orders.send_msg(Message::Log(format!(
                        "Error: position access denied: {}",
                        position_error.message()
                    )));
                }
                mk_geolocation::GeolocationPositionError::POSITION_UNAVAILABLE => {
                    orders.send_msg(Message::GpsContinuousStop());
                    orders.send_msg(Message::Log(format!(
                        "Error: position unavailable: {}",
                        position_error.message()
                    )));
                }
                mk_geolocation::GeolocationPositionError::TIMEOUT => {
                    model.gps_state = GpsState::Waiting;
                    orders.send_msg(Message::Log(format!(
                        "Error: position timeout: {}",
                        position_error.message()
                    )));
                }
                _ => {
                    orders.send_msg(Message::GpsContinuousStop());
                    orders.send_msg(Message::Log("Error: unkown position error".into()));
                }
            };
        }
        Message::InitMap(map) => {
            model.map = Some(map);
        }
        Message::TogglePreferences() => {
            model.ui_state = model.ui_state.toggle_preferences();
        }
        Message::ToggleTrackList() => {
            model.ui_state = model.ui_state.toggle_track_list();
        }
        Message::SelectTrack(show_track_index) => {
            let _ = model.load_track_from_db(show_track_index);
            orders.send_msg(Message::ToggleTrackList());
        }
        Message::ExportTrackAsGpx(index) => {
            if let Some(index) = index {
                if let Ok(gpx) = model.db.get(index) {
                    download_gpx(gpx);
                }
            }
        }
        Message::DbClear() => {
            let _ = model.db.clear();
        }
        Message::Log(message) => {
            model.log.push_front(message);
            model.log.truncate(30);
        }
    }
}

#[derive(Debug)]
pub(crate) enum Message {
    GpsOnOff(),
    ToggleGpsMode(),
    GpsContinuous(),
    GpsContinuousStop(),
    // gloo
    GeoPosition(mk_geolocation::GeolocationPosition),
    GeoPositionError(mk_geolocation::GeolocationPositionError),
    //
    InitMap(map::Map),
    // preferences
    TogglePreferences(),
    // track list
    ToggleTrackList(),
    SelectTrack(crate::db::SelectedDbIndex),
    ExportTrackAsGpx(Option<crate::db::SelectedDbIndex>),
    DbClear(),
    // log
    Log(String),
}
