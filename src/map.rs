// SPDX-License-Identifier: MIT
//
// TrailCorder Authors: see AUTHORS.txt

use seed::prelude::JsValue;
use serde::Serialize;

use mk_geolocation::GeolocationPosition;

#[derive(Debug)]
pub struct Map {
    pub map: leaflet::Map,
    pub trail_layer: leaflet::LayerGroup,
    pub trail_line: leaflet::Polyline,
    pub current_position: Option<PositionMarker>,
}

#[derive(Serialize)]
struct TileLayerOptions {
    pub attribution: String,
}

impl Map {
    pub fn new_on(div_id: &str) -> Self {
        let map = leaflet::Map::new(div_id, &JsValue::NULL);
        //map.setMaxZoom(18.0);

        let trail_layer = leaflet::LayerGroup::new();
        trail_layer.addTo(&map);

        let trail_line = leaflet::Polyline::new(vec![]);
        trail_line.addTo_LayerGroup(&trail_layer);

        let options = TileLayerOptions {
            attribution:
                "Map data © <a href=\"http://openstreetmap.org\">OpenStreetMap</a> contributors"
                    .into(),
        };
        leaflet::TileLayer::new(
            "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            &JsValue::from_serde(&options).unwrap_or(JsValue::NULL),
        )
        .addTo(&map);

        let last_pos = leaflet::LatLng::new(48.8, 9.0);
        map.setView(&last_pos, 12.0);

        Self {
            map,
            trail_layer,
            trail_line,
            current_position: None,
        }
    }

    pub fn add_position(&mut self, position: &GeolocationPosition) {
        let latlng =
            leaflet::LatLng::new(position.coords().latitude(), position.coords().longitude());
        // move to position
        self.map.setView(&latlng, self.map.getMaxZoom());
        // create position marker if it does not exist
        if self.current_position.is_none() {
            self.current_position = Some(PositionMarker::new_on(&latlng, &self.trail_layer));
        }
        // update position marker
        if let Some(marker) = &self.current_position {
            marker.update(&latlng, position.coords().accuracy());
        }
        // append position to track
        self.trail_line.addLatLng(&latlng);
        self.trail_line.redraw();
    }

    pub fn rebuild_from(&mut self, gpx_points: &[gpx::Waypoint]) {
        self.clear();

        for p in gpx_points {
            let latlng = leaflet::LatLng::new(p.point().y(), p.point().x());
            self.trail_line.addLatLng(&latlng);
        }
        self.trail_line.redraw();
        if !self.trail_line.isEmpty() {
            let center = self.trail_line.getCenter();
            // TODO: get bounds and calculate zoom level
            self.map.setView(&center, self.map.getMaxZoom());
        }
    }

    fn clear(&mut self) {
        // delete old trail line
        self.trail_line.remove();
        // delete position marker
        if let Some(marker) = &self.current_position {
            marker.remove();
        }
        // create empty trail line
        let trail_line = leaflet::Polyline::new(vec![]);
        self.trail_layer.addLayer(&trail_line);
        self.trail_line = trail_line;
    }
}

#[derive(Debug)]
pub struct PositionMarker {
    accuracy: leaflet::Circle,
    marker: leaflet::Marker,
}

impl PositionMarker {
    pub fn new_on(latlng: &leaflet::LatLng, layer: &leaflet::LayerGroup) -> Self {
        let circle = leaflet::Circle::new(latlng);
        circle.addTo_LayerGroup(&layer);
        let marker = leaflet::Marker::new(latlng);
        marker.addTo_LayerGroup(&layer);
        Self {
            accuracy: circle,
            marker,
        }
    }

    pub fn update(&self, latlng: &leaflet::LatLng, accuracy: f64) {
        let radius = accuracy / 2.;

        self.accuracy.setLatLng(&latlng);
        self.accuracy.setRadius(radius);

        self.marker.setLatLng(&latlng);
        self.marker.openPopup();
    }

    pub fn remove(&self) {
        self.accuracy.remove();
        self.marker.remove();
    }
}
