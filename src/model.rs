// SPDX-License-Identifier: MIT
//
// TrailCorder Authors: see AUTHORS.txt

use gpx::Gpx;
use mk_geolocation::GeolocationPosition;
use std::collections::VecDeque;

use crate::db::{Db, SelectedDbIndex};
use crate::geo::{GeoApiMode, Geolocator};
use crate::gpx::*;
use crate::map::Map;

pub const MAP_DIV_ID: &str = "trailcorder-map";

const DB_STORAGE_KEY: &str = "trailcorder-tracks";

#[derive(Debug, Copy, Clone)]
pub enum UiState {
    Map,
    Preferences,
    TrackList,
}

impl UiState {
    pub fn toggle_preferences(&self) -> UiState {
        match self {
            UiState::Preferences => UiState::Map,
            _ => UiState::Preferences,
        }
    }
    pub fn toggle_track_list(&self) -> UiState {
        match self {
            UiState::TrackList => UiState::Map,
            _ => UiState::TrackList,
        }
    }
    pub fn show_preferences(&self) -> bool {
        match self {
            UiState::Preferences => true,
            _ => false,
        }
    }
    pub fn show_track_list(&self) -> bool {
        match self {
            UiState::TrackList => true,
            _ => false,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum GpsState {
    Off,
    Waiting,
    On,
}

pub struct Model {
    pub geo_api_mode: GeoApiMode,
    pub geo_locator: Option<Box<dyn Geolocator>>,
    pub geo_options: mk_geolocation::PositionOptions,
    pub geo_pos_single_cmd_handle: Option<seed::app::CmdHandle>,
    pub gps_state: GpsState,
    // track
    pub gpx: Gpx,
    // db
    pub db: Db<Gpx>,
    // leaflet
    pub map: Option<Map>,
    //
    pub ui_state: UiState,
    // data
    pub log: VecDeque<String>,
}

impl Model {
    pub fn new(position_options: mk_geolocation::PositionOptions) -> Self {
        Self {
            geo_api_mode: GeoApiMode::Callback,
            geo_locator: None,
            geo_options: position_options,
            geo_pos_single_cmd_handle: None,
            gps_state: GpsState::Off,
            // track
            gpx: new_gpx(),
            // db
            db: Db::new(DB_STORAGE_KEY),
            // map
            map: None,
            //
            ui_state: UiState::Map,
            //
            log: VecDeque::with_capacity(31),
        }
    }

    pub fn add_current_position(&mut self, position: &GeolocationPosition) {
        self.gpx.tracks[0].segments[0]
            .points
            .push(geo_position_to_waypoint(position));
        self.map_add_position(&position);
    }

    pub fn store_track_in_db(&mut self) -> Result<(), crate::db::DbError> {
        self.db.upsert_entry(&self.gpx)
    }

    pub fn load_track_from_db(&mut self, index: SelectedDbIndex) -> Result<(), crate::db::DbError> {
        self.gpx = self.db.get(index)?.clone();
        self.map_rebuild();
        Ok(())
    }

    pub fn reset_track(&mut self) {
        self.gpx = new_gpx();
        self.map_rebuild();
    }

    fn map_rebuild(&mut self) {
        if let Some(map) = &mut self.map {
            map.rebuild_from(&self.gpx.tracks[0].segments[0].points);
        }
    }

    fn map_add_position(&mut self, position: &GeolocationPosition) {
        if let Some(map) = &mut self.map {
            map.add_position(position);
        }
    }
}
