// SPDX-License-Identifier: MIT
//
// TrailCorder Authors: see AUTHORS.txt

use seed::prelude::{LocalStorage, WebStorage};
use serde::{Deserialize, Serialize};

/// Index into the database
#[derive(Debug, Copy, Clone, Eq, PartialEq, PartialOrd)]
pub struct SelectedDbIndex(pub usize);

impl From<usize> for SelectedDbIndex {
    fn from(val: usize) -> Self {
        Self(val)
    }
}

impl std::ops::Add<usize> for SelectedDbIndex {
    type Output = Self;

    fn add(self, rhs: usize) -> Self {
        Self(self.0 + rhs)
    }
}

impl std::ops::AddAssign<usize> for SelectedDbIndex {
    fn add_assign(&mut self, rhs: usize) {
        self.0 += rhs;
    }
}

/// Comparison trait used for ordering entries when inserting into the database
pub trait DbKeyComp {
    /// This Trait mus be implemented for elements stored in the DB
    /// and returns an [`Ordering`] between `self` and `other`.
    ///
    /// By convention, `self.cmp(&other)` returns the ordering matching the expression
    /// `self <operator> other` if true.
    ///
    /// # Example: which Ordering value to return
    ///
    /// ```
    /// use std::cmp::Ordering;
    ///
    /// assert_eq!(5.cmp(&10), Ordering::Less);
    /// assert_eq!(10.cmp(&5), Ordering::Greater);
    /// assert_eq!(5.cmp(&5), Ordering::Equal);
    /// ```
    #[must_use]
    fn db_key_cmp(&self, other: &Self) -> std::cmp::Ordering;
}

/// possible errors encountered when calling methods on the database instance
#[derive(Debug, Copy, Clone)]
pub enum DbError {
    /// entry at index is not found
    MissingEntry(SelectedDbIndex),
    /// error accessing the underlying storage
    StorageAccess,
}

impl std::fmt::Display for DbError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            DbError::MissingEntry(index) => {
                write!(f, "Entry {:?} does not exist in database", index)
            }
            DbError::StorageAccess => {
                write!(f, "Error while accessing storage")
            }
        }
    }
}

// allow conversion into Error
impl std::error::Error for DbError {}

impl From<seed::prelude::web_storage::WebStorageError> for DbError {
    fn from(_: seed::prelude::web_storage::WebStorageError) -> Self {
        DbError::StorageAccess
    }
}

/// database Result alias
pub type DbResult<T> = std::result::Result<T, DbError>;

/// Database structure
#[derive(Debug)]
pub struct Db<D>
where
    for<'de> D: DbKeyComp + Clone + Serialize + Deserialize<'de>,
{
    data: Vec<D>,
    table_name: &'static str,
}

impl<D> Db<D>
where
    for<'de> D: DbKeyComp + Clone + Serialize + Deserialize<'de>,
{
    /// create new database which stores all data in `table_name`
    pub fn new(table_name: &'static str) -> Self {
        let data = LocalStorage::get(table_name).unwrap_or_else(|_| vec![]);
        Self { data, table_name }
    }

    /// get all data as slice of the contained entries
    pub fn get_all(&self) -> &[D] {
        &self.data
    }

    /// get not more then the last `max_entries` entries
    pub fn _get_latest_entries(&self, max_entries: usize) -> &[D] {
        let index: usize = if self.data.len() < max_entries {
            0
        } else {
            self.data.len() - max_entries
        };
        &self.data[index..]
    }

    /// get entry at `index`
    /// returns an error if the element is not found
    pub fn get(&self, index: SelectedDbIndex) -> DbResult<&D> {
        self.data.get(index.0).ok_or(DbError::MissingEntry(index))
    }

    /// return the number of entries in the database
    pub fn _entry_count(&self) -> usize {
        self.data.len()
    }

    /// insert or update `entry` and persist database
    pub fn upsert_entry(&mut self, entry: &D) -> DbResult<()> {
        self.upsert_entry_into_vec(entry);
        self.store_data()
    }

    /// clear all entries and persist database
    pub fn clear(&mut self) -> DbResult<()> {
        self.data.clear();
        self.store_data()
    }

    /// persist data in browser localstorage
    fn store_data(&self) -> DbResult<()> {
        LocalStorage::insert(self.table_name, &self.data)?;
        Ok(())
    }

    /// insert or update entry into the cache array in timely order
    /// based on DbKeyComp trait
    fn upsert_entry_into_vec(&mut self, entry: &D) {
        let pos = self.data.binary_search_by(|other| other.db_key_cmp(&entry));
        match pos {
            Ok(pos) => self.data[pos] = entry.clone(),
            Err(pos) => self.data.insert(pos, entry.clone()),
        }
    }
}
