// SPDX-License-Identifier: MIT
//
// TrailCorder Authors: see AUTHORS.txt

use seed::prelude::*;
use seed::*;
use std::collections::VecDeque;

use crate::geo::GeoApiMode;
use crate::model::GpsState;

pub(crate) fn trailcorder_main<M: 'static>(
    model: &crate::model::Model,
    map_div_id: &str,
    on_gps_on_off: impl FnOnce() -> M + 'static + Clone,
    // preferences
    on_preferences_toggle: impl FnOnce() -> M + 'static + Clone,
    on_toggle_gps_api_mode: impl FnOnce() -> M + 'static + Clone,
    // track list
    on_track_list_toggle: impl FnOnce() -> M + 'static + Clone,
    on_track_list_selected: impl FnOnce(crate::db::SelectedDbIndex) -> M + 'static + Clone,
    on_track_list_export: impl FnOnce(Option<crate::db::SelectedDbIndex>) -> M + 'static + Clone,
    on_db_clear: impl FnOnce() -> M + 'static + Clone,
) -> Node<M> {
    let grid_style = style! {
        St::Display => "grid";
        St::Width => "100%";
        St::Height => "100%";
        St::Padding => 0;
        St::Margin => 0;
        St::GridTemplateColumns => "minmax(0, 1fr)";
        St::GridTemplateRows => "minmax(0, 1fr) 2px minmax(0, min-content) ";
        St::RowGap => "0";
        St::ColumnGap => "0";
        St::AlignItems => "center";
    };

    div![div![
        grid_style,
        // map view
        div![
            style! {
                St::Padding => 0;
                St::Margin => 0;
                St::Width => "100%";
                St::Height => "100%";
                St::BoxSizing => "border-box";
                St::GridRowStart => "1";
                St::GridRowEnd => "1";
                St::GridColumnStart => "1";
                St::GridColumnEnd => "1";
                St::ZIndex => "1";
            },
            attrs![
                At::Id => map_div_id,
            ],
        ],
        // preferences
        preferences_view(
            model.ui_state.show_preferences(),
            &model.geo_api_mode,
            &model.db,
            log_messages_to_view(&model.log),
            on_toggle_gps_api_mode,
        ),
        // trail list
        trail_list_view(
            &model.db,
            on_track_list_selected,
            on_track_list_export,
            on_db_clear,
            model.ui_state.show_track_list(),
        ),
        // toggle preferences
        div![
            style! {
                St::Width => "100%",
                St::GridRowStart => "3";
                St::GridRowEnd => "3";
                St::GridColumnStart => "1";
                St::GridColumnEnd => "1";
                //
                St::Display => "flex";
                St::FlexFlow => "row";
                //
                St::ZIndex => "1";
            },
            input![
                style! {
                    St::BackgroundColor => "grey" ,
                    St::FontSize => "200%",
                    St::TextAlign => "center",
                    St::Flex => "1 1 auto";
                    St::ZIndex => "2";
                },
                attrs![
                    At::Id => "trailcorder-preferences-toggle",
                    At::Type => "button",
                    At::Value => if model.ui_state.show_preferences() { "\u{274C}" } else { "\u{2699}" },
                ],
                mouse_ev(Ev::Click, move |_| on_preferences_toggle(),),
            ],
            input![
                style! {
                    St::BackgroundColor => match model.gps_state {
                        GpsState::Off => "red",
                        GpsState::Waiting => "orange",
                        GpsState::On => "green",
                    },
                    St::FontSize => "200%",
                    St::TextAlign => "center",
                    St::Flex => "1 1 auto";
                    St::ZIndex => 2,

                },
                attrs![
                    At::Id => "trailcorder-gps-status",
                    At::Type => "button",
                    At::Value => "\u{1F310}",
                ],
                mouse_ev(Ev::Click, move |_| on_gps_on_off(),),
            ],
            input![
                style! {
                    St::BackgroundColor => "grey" ,
                    St::FontSize => "200%",
                    St::TextAlign => "center",
                    St::Flex => "1 1 auto";
                    St::ZIndex => "2";
                },
                attrs![
                    At::Id => "trailcorder-track-list-toggle",
                    At::Type => "button",
                    At::Value => if model.ui_state.show_track_list() { "\u{274C}" } else { "\u{1F6E4}" },
                ],
                mouse_ev(Ev::Click, move |_| on_track_list_toggle(),),
            ],
        ],
    ],]
}

fn preferences_view<M: 'static>(
    show_preferences: bool,
    geo_mode: &GeoApiMode,
    _db: &crate::db::Db<gpx::Gpx>,
    log_content: Node<M>,
    on_toggle_gps_api_mode: impl FnOnce() -> M + 'static + Clone,
) -> Node<M> {
    let grid_style = style! {
        St::GridRowStart => "1";
        St::GridRowEnd => "2";
        St::GridColumnStart => "1";
        St::GridColumnEnd => "1";
        St::ZIndex => "2";
        //
        St::Display => if show_preferences { "grid" } else { "none" };
        St::BackgroundColor => "white";
        St::Width => "100%";
        St::Height => "100%";
        //
        St::GridTemplateColumns => "minmax(0, 1fr)";
        St::GridTemplateRows => "minmax(0, min-content) minmax(0, 1fr) ";
        St::RowGap => "1em";
        St::ColumnGap => "1em";
        St::AlignItems => "center";
    };
    let input_style = style! {
        St::Padding => 0;
        St::Margin => 0;
        St::BoxSizing => "border-box";
        St::FontSize => "230%";
        St::Width => "100%";
        St::Border => "1px solid #CCC",
        St::BorderRadius => "0.25em",
    };

    let id = "trailcorder-preferences";
    let geo_mode_str = if GeoApiMode::Callback == *geo_mode {
        "Callback API"
    } else {
        "Futures API"
    };

    div![
        grid_style,
        attrs![
            At::Id => format!("{}", id),
        ],
        input![
            input_style.clone(),
            attrs![
                At::Id => format!("{}-api-toggle", id),
                At::Type => "button",
                At::Value => geo_mode_str,
            ],
            mouse_ev(Ev::Click, move |_| on_toggle_gps_api_mode(),),
        ],
        log_content,
    ]
}

fn log_messages_to_view<M: 'static>(log: &VecDeque<String>) -> Node<M> {
    div![
        style! {
            St::Width => "100%",
            St::Height => "100%",
            St::OverflowY => "auto",
        },
        log.iter().map(|c| div![c]).collect::<Vec<Node<_>>>()
    ]
}

fn trail_list_view<M: 'static>(
    db: &crate::db::Db<gpx::Gpx>,
    on_track_list_selected: impl FnOnce(crate::db::SelectedDbIndex) -> M + 'static + Clone,
    on_track_list_export: impl FnOnce(Option<crate::db::SelectedDbIndex>) -> M + 'static + Clone,
    on_db_clear: impl FnOnce() -> M + 'static + Clone,
    show: bool,
) -> Node<M> {
    let grid_style = style! {
        St::GridRowStart => "1";
        St::GridRowEnd => "3";
        St::GridColumnStart => "1";
        St::GridColumnEnd => "1";
        St::ZIndex => "2";
        //
        St::Display => if show { "flex" } else { "none" };
        St::FlexFlow => "column",
        //
        St::BackgroundColor => "orange";
        St::Width => "100%";
        St::Height => "100%";
    };

    let input_style = style! {
        St::Padding => 0;
        St::Margin => 0;
        St::BoxSizing => "border-box";
        St::FontSize => "230%";
        St::Width => "100%";
        St::Border => "1px solid #CCC",
        St::BorderRadius => "0.25em",
    };

    let db_elements = db.get_all();
    let db_indices: Vec<crate::db::SelectedDbIndex> =
        (0..db_elements.len()).map(|i| i.into()).collect();

    div![
        grid_style,
        p![
            style! {
                St::Margin => "0.5em 0",
                St::Flex => "0 1 auto",
                St::TextAlign => "center";
                St::FontSize => "200%",
            },
            b!["Recorded Trails"],
        ],
        div![
            style! {
                St::Width => "100%",
                St::Display => "flex",
                St::FlexFlow => "column",
                St::Flex => "1 1 auto",
                St::OverflowY => "auto",
            },
            db_indices
                .iter()
                .zip(db_elements.iter())
                .map(|(&index, ref track)| {
                    let load_track_cb = on_track_list_selected.clone();
                    let export_track_cb = on_track_list_export.clone();
                    let index = index.clone();
                    let timestamp = track.metadata.as_ref().unwrap().time.unwrap();
                    div![
                        style! {
                            St::Width => "100%",
                            St::Display => "flex",
                            St::FlexFlow => "row",
                            St::Margin => "0.25em 0",
                            St::Padding => "0.25em 0",
                            St::Background => "darkorange",
                        },
                        div![
                            style! {
                                St::Width => "100%",
                                St::TextAlign => "center",
                                St::FontSize => "200%",
                            },
                            timestamp.to_string(),
                            ev(Ev::Click, move |_| load_track_cb(index),),
                        ],
                        div![
                            style! {
                                St::TextAlign => "center",
                                St::Background => "darkcyan",
                                St::FontSize => "200%",
                                St::Margin => "0 0.2em",
                                St::Padding => "0 0.2em",
                            },
                            "\u{1F4E5}",
                            ev(Ev::Click, move |_| export_track_cb(Some(index)),),
                        ],
                    ]
                })
                .collect::<Vec<Node<_>>>()
        ],
        input![
            input_style.clone(),
            attrs![
                At::Id => format!("trailcorder-db-clear"),
                At::Type => "button",
                At::Value => "Delete All Trails",
            ],
            mouse_ev(Ev::Click, move |_| on_db_clear(),),
        ],
    ]
}
