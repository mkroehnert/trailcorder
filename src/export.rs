// SPDX-License-Identifier: MIT
//
// TrailCorder Authors: see AUTHORS.txt

use seed::prelude::{js_sys, web_sys, JsCast};

use chrono::Utc;

pub fn download_gpx(gpx: &gpx::Gpx) {
    let mut data: Vec<u8> = Vec::new();
    gpx::write(&gpx, &mut data).unwrap();

    let encoded_data: String =
        js_sys::encode_uri_component(std::str::from_utf8(&data).unwrap()).into();
    let mime = String::from("data:text/plain;charset=utf-8");

    let timestamp = gpx
        .metadata
        .as_ref()
        .unwrap_or(&gpx::Metadata {
            time: Some(Utc::now()),
            ..Default::default()
        })
        .time
        .unwrap_or(Utc::now());

    let filename = timestamp.format("%Y-%m-%d_%H-%M-track.gpx").to_string();

    download_data_uri_as(&format!("{},{}", mime, encoded_data), &filename);
}

fn download_data_uri_as(data_uri: &str, filename: &str) {
    let element = seed::document()
        .create_element("a")
        .expect("should be able to create element");

    let _ = element.set_attribute("href", data_uri);
    let _ = element.set_attribute("download", filename);

    let event = seed::document()
        .create_event("MouseEvents")
        .expect("should be able to call createEvent()")
        .dyn_into::<web_sys::MouseEvent>()
        .ok()
        .expect("should be a MouseEvent");
    event.init_mouse_event_with_can_bubble_arg_and_cancelable_arg("click", true, true);
    let _ = element.dispatch_event(&event);

    element.remove();
}
