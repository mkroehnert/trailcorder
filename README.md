ABOUT
-----

TrailCorder is a single page webapp which displays the current GPS location in your browser via Openstreetmap.

It is built using [Rust](https://rust-lang.org/), [Seed](https://seed-rs.org), and [leaflet-rs](https://github.com/kartevonmorgen/leaflet-rs).


REQUIREMENTS
------------

Any modern browser that supports WebAssembly and the Geolocation API.


DOWNLOADING
-----------

The sourcecode can be downloaded from the GitLab repository:

https://gitlab.com/mkroehnert/trailcorder


BUILDING
--------

Install the following tools

* rustup
* cargo install cargo-make
* cargo install wasm-pack

Building for the web

* cargo make build


LICENSE
-------

The TrailCorder sourcecode is licensed under the MIT license (see [LICENSE.txt](LICENSE.txt)).

* [seed (MIT)](https://seed-rs.org)
* [serde (MIT/Apache-2.0)](https://crates.io/crates/serde)
* [mk-geolocation (MIT/Apache-2.0)](https://gitlab.com/mkroehnert/mk-geolocation)
* [leaflet-rs (MIT/Apache-2.0)](https://github.com/kartevonmorgen/leaflet-rs)
