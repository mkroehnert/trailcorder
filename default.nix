# run with nix-shell .
with import <nixpkgs> {}; {
    wasmEnv = stdenv.mkDerivation {
        name = "rust-wasm";
        buildInputs = [ clangStdenv openssl pkgconfig binutils zlib sqlite zip ];

#      shellHook =
#      ;
#        extraCmds = ''
#        '';
    };
}
